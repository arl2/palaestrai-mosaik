Configuration
=============


Have a look in the source code under tests/example_experiment_midas.yml.
The configuration is not final, yet, but this example file should work
if you have installed everything (midas, pysimmods, palaestrAI, and
palaestrAI-mosaik).

We will extend this guide in the near future.
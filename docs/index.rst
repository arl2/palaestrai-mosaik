.. palaestrAI Mosaik Interface documentation master file, created by
   sphinx-quickstart on Tue Sep 29 08:43:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to palaestrAI Mosaik Interface's documentation!
=======================================================

This module provides an ARL environment to couple mosaik simulations and ARL. 


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   getting_started
   configuration
   midas_example
   api/modules
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

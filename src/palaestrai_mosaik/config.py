ENVIRONMENT_LOGGER_NAME = "palaestrai_mosaik"
SIMULATOR_LOGGER_NAME = "palaestrai_mosaik.simulator"
DATE_FORMAT = "%Y-%m-%d %H:%M:%S%z"
